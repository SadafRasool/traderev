package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


/*
 * Class Name: Home.java
 * 
 * 
 * This Page contains all the WebElements for Home Page which are being used in different tests
   The WebElements have been identified using either Xpath or CSSSelector
 *
 *
 *
 */

public class Home {
	
	
	
	
	WebDriver driver;
	
	@FindBy(how=How.CSS, using="a.site-header__logo")
	@CacheLookup
	public WebElement logo_home;
	
	@FindBy(how=How.CSS, using="a[href='https://work.traderev.com/']")
	@CacheLookup
	public WebElement link_careers;
	
	
	
	public Home(WebDriver driver) {
		this.driver = driver;
	}
	
	
	
	
	

}
