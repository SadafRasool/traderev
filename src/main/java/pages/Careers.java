package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



/*
 * Class Name: Careers.java
 * 
 * 
 * This Page contains all the WebElements for Careers Page which are being used in different tests
   The WebElements have been identified using either Xpath or CSSSelector
*
*
*
*/

public class Careers {
	
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "(//a[@href='http://work.traderev.com/regionone/'])[2]")
	public WebElement logoCareerPage;
	
	@FindBy(how = How.CSS, using = "a[title='Canadian Jobs']")
	public WebElement linkCanadianOpportunities;
	
	public Careers(WebDriver driver) {
		this.driver = driver;
	}
	
	

}
