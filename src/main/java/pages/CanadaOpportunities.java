package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


/*
 * Class Name: CanadaOpportunitites.java
 * 
 * 
 * This Page contains all the WebElements for Canada Opportunitites Page which are being used in different tests
   The WebElements have been identified using either Xpath or CSSSelector
*
*
*
*/

public class CanadaOpportunities {
	
	private WebDriver driver;
	private CanadaOpportunities canadaOpportunities;
	private List<WebElement> allJobs; 
	
	@FindBy(how=How.XPATH, using="//span[contains(text(),'Filter by')]")
	public WebElement labelFilterBy;
	
	@FindBy(how=How.XPATH, using="//div[text()='City']")
	public WebElement divCity;
	
	@FindBy(how=How.XPATH, using="//a[@href='?location=Toronto%2C%20Ontario%2C%20Canada']")
	public WebElement linkToronto;
	
	@FindBy(how=How.XPATH, using = "(//div[@class='posting-categories'])")
	public List<WebElement> divJobPosting;
	
	@FindBy(how=How.XPATH, using = "//div[text()='Team']")
	public WebElement divTeam;
	
	@FindBy(how=How.XPATH, using="//a[text()='Engineering']")
	public WebElement linkEngineeringTeam;
	
		
	public CanadaOpportunities(WebDriver driver) {
		this.driver = driver;
	}

}
