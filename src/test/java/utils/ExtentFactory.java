package utils;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Properties;

import com.relevantcodes.extentreports.ExtentReports;

import tests.FirstScenario;


/*
 * Class Name: ExtentFactory.java
 * 
 * 
 *This is factory class for extent report which is leveraged by different test classes for logging purpose
 *
 *
 *
 */

public class ExtentFactory {	
	static ExtentReports extentReports;	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	public static String finalPath = "test-output/Reports/" + "Result_" + getTimeStamp() + ".html";
	
	
	//Method used to create timeStamp
	static String getTimeStamp() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String time = sdf.format(timestamp);
		return time;
	}
	
	
	//Factroy Method to get the same Reporter Instance 
	public static ExtentReports getReporterInstance(){	
		extentReports = new ExtentReports(finalPath, false);
		return extentReports;		
	}
	
}
