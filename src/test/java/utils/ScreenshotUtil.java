package utils;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import tests.FirstScenario;

/*
 * Class Name: ScreenshotUtil.java
 * 
 * 
 * This is a util class for logging into extent report with a screenshot
 *
 *
 *
 */


public class ScreenshotUtil {
	
	static File destFile;
	
	
	
	public static void logStatusWithScreenShot(WebDriver driver, ExtentTest test, String fileName, LogStatus logStatus, String msg) {
		Properties properties = new Properties();
		try {
			InputStream input = FirstScenario.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(input);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		String screenshotPath = properties.getProperty("screenshotpath");		
		destFile = new File(screenshotPath + fileName + ".JPEG");	
		try {
			FileUtils.copyFile(((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE), destFile); 
			test.log(logStatus, msg + test.addScreenCapture(destFile.getAbsolutePath()));
		}catch(Exception e) {
			e.printStackTrace();
		}	
		
	}

}
