package tests;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import pages.CanadaOpportunities;
import utils.ExtentFactory;
import utils.ScreenshotUtil;



/*
 * Class Name: ThirdScenario.java
 * 
 * 
 * This is a test class which will validate the third scenario as per the description
 *
 * This test could be executed as an individual test as well as with test suite
 *
 * Dependendcy - None
 * Pre-requisite - None
 *
 */
public class ThirdScenario {
	
	private WebDriver driver;
	private CanadaOpportunities canadaOpportunities;
	
	private int allTorontoEngineeringJobsCount;
	private String jobLocation;
	private String jobTeam;
	private boolean invalidJobLocation;
	private boolean invalidJobTeam;
	
	private ExtentReports reports;
	private ExtentTest test;
	
	private String jobUrl;
	private String driverPath;
	private int waitTime;
	private String jobLocationSearch;
	private String jobtype;
	
	@BeforeClass
	void setUp() {
		reports = ExtentFactory.getReporterInstance();
		
		Properties properties = new Properties();
		try {
			InputStream input = FirstScenario.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(input);
		}catch(Exception e) {
			e.printStackTrace();
		}
				
		driverPath = properties.getProperty("chromepath");
		jobUrl = properties.getProperty("joburl");
		waitTime = Integer.parseInt(properties.getProperty("wait"));
		jobLocationSearch = properties.getProperty("joblocation");	
		jobtype = properties.getProperty("jobtype");
		
		
		System.setProperty("webdriver.chrome.driver", driverPath);    
    	ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", false);
		options.addArguments("start-maximized");
    	driver = new ChromeDriver(options);	
    	driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
    	canadaOpportunities = PageFactory.initElements(driver, CanadaOpportunities.class);
    	driver.get(jobUrl); 
	}
	
	//Test to validate the search functionality for given city and job type
	@Test(priority=0)
	void searchForTorontoEngineeringJobs() {
		test = reports.startTest("ThirdTestScenario");
		if (canadaOpportunities.divCity.isDisplayed()) {
			test.log(LogStatus.PASS, "Successfully navigated to TradeRev Jobs Page");
			test.log(LogStatus.INFO, "Searching for all the Engineering Jobs in Toronto");
			canadaOpportunities.divCity.click();
			canadaOpportunities.linkToronto.click();
			canadaOpportunities.divTeam.click();
			canadaOpportunities.linkEngineeringTeam.click();
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobSearchPass", LogStatus.PASS, "Selected the city as Torornto and Job Type as Engineering");
		}else {
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobSearchPass", LogStatus.FAIL, "Could not be navigated to TradeRev Job Page");
		}
		
		//Validating the search results show the job location and job type as per the search criteria
		allTorontoEngineeringJobsCount = canadaOpportunities.divJobPosting.size();
		
		//Validating if search results have displayed the jobs
		if(allTorontoEngineeringJobsCount>0) {			
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobSearchCountPass", LogStatus.PASS, "There are total " + allTorontoEngineeringJobsCount + " Engineering jobs open in Toronto.");
			test.log(LogStatus.INFO, "Verifying that all jobs belong to Toronto Engineering Team");
			
			//Validating all the returned jobs have the desired location and job type
			for(WebElement job : canadaOpportunities.divJobPosting) {
				jobLocation = job.findElement(By.xpath(".//span[1]")).getText();
				jobTeam = job.findElement(By.xpath(".//span[2]")).getText();
				if(!(jobLocation.equalsIgnoreCase(jobLocationSearch))) {
					invalidJobLocation = true;
					break;
				}
				
				if(!((jobTeam.toUpperCase()).contains(jobtype.toUpperCase()))) {
					invalidJobTeam = true;
					break;
				}
			}
			
			//Validating and Reporting
			if(invalidJobLocation || invalidJobTeam ) {				
				ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobSearchLocationFail", LogStatus.FAIL, "There is at least one job which does not have the right location OR right team");		
			}else {				
				ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobSearchLocationPass", LogStatus.PASS, "Successfully verified that all jobs have location as 'Toronto, Ontario, Canada' and Team as 'Engineering'");
			}
		}else{ //In case there was no job found based on search criteria
			test.log(LogStatus.INFO, "There are no Engineering jobs available for Toronto location");
		}
		reports.endTest(test);
		reports.flush();
	}
		
	//This AfterSuite Method will put the result file on browser when third and final scenario is completed
	@AfterSuite
	void showReport() {		
		String resultFile = ExtentFactory.finalPath;
		File file = new File(resultFile);
		String filePath = file.getAbsolutePath();
		driver.get("file:///" + filePath);
	}
}
