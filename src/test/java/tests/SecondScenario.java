package tests;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import pages.CanadaOpportunities;
import utils.ExtentFactory;
import utils.ScreenshotUtil;



/*
 * Class Name: SecondScenario.java
 * 
 * 
 * This is a test class which will validate the third scenario as per the description
 * This test could be executed as an individual test as well as with test suite
 *
 * Dependendcy - None
 * Pre-requisite - None
 *
 *
 */




public class SecondScenario {
	
	private WebDriver driver;
	private CanadaOpportunities canadaOpportunities;
	
	private int allTorontoJobsCount;
	private String jobLocation;
	private boolean invalidJobLocation;
	
	private ExtentReports reports;
	private ExtentTest test;
	
	private String jobUrl;
	private String driverPath;
	private int waitTime;
	private String jobLocationSearch;

	//Set Up
	@BeforeClass
	void setUp() {
		reports = ExtentFactory.getReporterInstance();
		Properties properties = new Properties();
		try {
			InputStream input = FirstScenario.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(input);
		}catch(Exception e) {
			e.printStackTrace();
		}
				
		driverPath = properties.getProperty("chromepath");
		jobUrl = properties.getProperty("joburl");
		waitTime = Integer.parseInt(properties.getProperty("wait"));
		jobLocationSearch = properties.getProperty("joblocation");	
	
		System.setProperty("webdriver.chrome.driver", driverPath);
    	ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", false);
		options.addArguments("start-maximized");
    	driver = new ChromeDriver(options);	
    	driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
    	canadaOpportunities = PageFactory.initElements(driver, CanadaOpportunities.class);
    	driver.get(jobUrl);
	}
	
		
	//Test to validate the search results based on job location
	@Test(priority=1)
	void validateAllTorontoJobs() {
		test = reports.startTest("SecondTestScenario");		
		
		//Putting the search criteria
		canadaOpportunities.divCity.click();
		canadaOpportunities.linkToronto.click();
		
		//Checking the total no of items returned
		if(canadaOpportunities.divJobPosting.size()>0) {}
		test.log(LogStatus.INFO, "Searching for all jobs opportunities in Toronto");		
		allTorontoJobsCount = canadaOpportunities.divJobPosting.size();
		if(allTorontoJobsCount>0) {			
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobSearch", LogStatus.PASS, "Successfully searched for all Toronto Jobs");		
			test.log(LogStatus.INFO, "There are total " + allTorontoJobsCount + " jobs open in Toronto.");
			test.log(LogStatus.INFO, "Validating the location of all jobs.");
			
			//Validating all job locations on the page should have desired location
			for(WebElement job : canadaOpportunities.divJobPosting) {
				jobLocation = job.findElement(By.xpath(".//span[1]")).getText();
				if(!(jobLocation.equalsIgnoreCase(jobLocationSearch))) {
					invalidJobLocation = true;
					break;
				}
			}			
			
			//Validating and reporting if there is any job with job location other than Toronto
			if(invalidJobLocation) {					
				ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobLocationFail", LogStatus.FAIL, "There is at least one job which does not have the right location");				
			}else {			
				ScreenshotUtil.logStatusWithScreenShot(driver, test, "JobLocationPass", LogStatus.PASS, "All jobs having 'Toronto, Ontario, Canada' as their location");
			}			
		}else {		//In case there was no job found in Toronto
			test.log(LogStatus.INFO, "There are no jobs available for Toronto location");
		}
		reports.endTest(test);
		reports.flush();
	}
	

	@AfterClass
	void tearDown() {
		reports.endTest(test);
	}
}
