package tests;

import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import pages.CanadaOpportunities;
import pages.Careers;
import pages.Home;
import utils.ExtentFactory;
import utils.ScreenshotUtil;



/*
 * Class Name: FirstScenario.java
 * 
 * 
 * This is a test class which will validate the first scenario as per the description
 * This test could be executed as an individual test as well as with test suite
 *
 * Dependendcy - None
 * Pre-requisite - None
 *
 *
 */
public class FirstScenario {
	private WebDriver driver;
	private Home home;
	private Careers careers;
	private CanadaOpportunities canadaOpportunities;
	
	
	private String homeWindow;
	private String careerWindow;
	private Set<String> allWindows;
	
	private ExtentReports reports;
	private ExtentTest test;
	
	private String homeUrl;
	private String driverPath;
	private int waitTime;
	
	//Setting up		
	@BeforeTest
	void setUp() {	
		reports = ExtentFactory.getReporterInstance();
		Properties properties = new Properties();
		try {
			InputStream input = FirstScenario.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(input);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		driverPath = properties.getProperty("chromepath");
		homeUrl = properties.getProperty("homeurl");
		waitTime = Integer.parseInt(properties.getProperty("wait"));
	
		System.setProperty("webdriver.chrome.driver", driverPath);
    	ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", false);
		options.addArguments("start-maximized");
    	driver = new ChromeDriver(options);	
    	driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
    	driver.get(homeUrl);
    	home = PageFactory.initElements(driver, Home.class);
    	careers = PageFactory.initElements(driver, Careers.class);
    	canadaOpportunities = PageFactory.initElements(driver, CanadaOpportunities.class);
	}
	
	
	
	//Test to validate Home Page Navigation
	@Test(priority=0)
	void validateHomePage() {
		test = reports.startTest("FirstTestScenarion");
		homeWindow = driver.getWindowHandle();
		boolean homePageDisplayed = home.logo_home.isDisplayed();
		if(homePageDisplayed) {			  //Validating Home Page Displayed	
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "HomePage", LogStatus.PASS, "TradeRev Home Page is displayed succesfully");
		}else {
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "HomePage", LogStatus.FAIL, "TradeRev Home page is not displayed");
		}
		
		
		//Navigating to Careers Page
		home.link_careers.click();
		test.log(LogStatus.INFO, "Navigating to Careers Page");
		allWindows = driver.getWindowHandles();
		
		for(String currentWindow : allWindows ) {			
			if(!(currentWindow.equalsIgnoreCase(homeWindow))) {
				careerWindow = currentWindow;
				driver.switchTo().window(currentWindow);
			}
		}		
		
		//Validating Careers Page is displayed
		if(careers.logoCareerPage.isDisplayed()) {
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "CareersPage", LogStatus.PASS, "TradeRev Careers Page Navigated Successfully");		
		}else {
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "CareersPage", LogStatus.FAIL, "TradeRev Careers Page Could Not Be Navigated Successfully");
		}
		
		
		//Navigating to Canada Opportunities Page
		careers.linkCanadianOpportunities.click();  
		test.log(LogStatus.INFO, "Navigating to Canadian Opportunities Page");
		allWindows = driver.getWindowHandles();
		for(String currentWindow : allWindows) {
			if(!(currentWindow.equalsIgnoreCase(homeWindow)&&currentWindow.equalsIgnoreCase(careerWindow))) {
				driver.switchTo().window(currentWindow);
			}
		}
		
		//Validating Canada Opportunities Page is Displayed
		if(canadaOpportunities.labelFilterBy.isDisplayed()) {			
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "CanadianOpportunities", LogStatus.PASS, "Canadian Opportunities Page Navigated Successfully");
		}else {
			ScreenshotUtil.logStatusWithScreenShot(driver, test, "CanadianOpportunities", LogStatus.FAIL, "Canadian Opportunities Page Could Not Be Navigated Successfully");
		}
		reports.endTest(test);
		reports.flush();		
	}
	
	@AfterTest
	void tearDown() {
		driver.quit();
	}
}
