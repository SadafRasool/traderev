############################################Read Me################################################################


Project Name - TradeRev Automation Suite
Submitted By - Sadaf Rasool

Technology Stack:
Java 				1.8
Selenium 			3.10.0
Maven version 		3.5.4
Chrome Version 		79.0.3945.117
TestNG 				6.14.3
ExtentReports		2.41.2


Frameworks Used:
Hybrid (Page Object Model + Test NG)


How to run the Automation Suite:
Option 1: 
A) Get a copy on your local machine. 
git clone https://SadafRasool@bitbucket.org/SadafRasool/traderev.git 
B) Launch the command prompt/terminal 
C) cd to the project directory which is your/local/path/automation-suite 
C) Run the mvn test command i.e.  mvn clean test
		
		OR
		
Option 2:
A) Get the clone on your local machine. 
B) Launch your IDE for java projects
B) import the project as java-maven project 
C) Once imported, locate the TradeRevAutomationSuite.xml
D) Right click and run as TestNg Suite 

		OR

Option 3:
A) Get the clone on your local machine. 
B) Launch your IDE for java projects
B) import the project as java-maven project 
C) Once imported, locate the TestRunner.java in src/test/java/tests
D) Run this java class as Java Application


Test Report Path:
your/project/location/test-output/Reports

Test Screen Shots Path:
your/project/location/test-output/Reports/screenshots



Please Note :  There would be result file opens on your browser once test suite execution is finished.



##########################################Thanks#############################################################